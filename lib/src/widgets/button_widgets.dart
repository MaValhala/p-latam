import 'package:flutter/material.dart';
var colors = {
    "1": Color.fromRGBO(242, 27, 87, 1),
    "2": Color.fromRGBO(122, 43, 134, 1),
    "3": Color.fromRGBO(250, 188, 17, 1),
    "4": Color.fromRGBO(21, 171, 159, 1),
    "5": Color.fromRGBO(43, 120, 226, 1),
    "6": Color.fromRGBO(237, 74, 74, 1)
  };
Widget backButtom(BuildContext context, String area) {
    final size = MediaQuery.of(context).size;
    return FloatingActionButton(
      child: Icon(Icons.arrow_back_ios,
                color: Colors.white,
                size: size.width*0.04,
      ),
      onPressed: () {
        Navigator.pop(context);
      },
      backgroundColor: colors[area]==null?Color.fromRGBO(237, 74, 74, 1):colors[area],
    );
}

  Widget bottomLogo(final size){
    return Container(
      height: size.height*0.05,
      child: Image(
      image: AssetImage('assets/latam/bottomLogo.png'),
      fit: BoxFit.cover,
    ),
    );
  }