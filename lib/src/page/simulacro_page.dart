import 'dart:async';
// import 'dart:io';
// import 'dart:js';

import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:xplora/src/bloc/login_bloc.dart';
import 'package:xplora/src/models/simulacro_model.dart';
import 'package:xplora/src/providers/factions_provider.dart';
import 'package:xplora/src/providers/login_provider.dart';
import 'package:xplora/src/providers/profile_provider.dart';
import 'package:xplora/src/providers/simulacro_provider.dart';
import 'package:xplora/src/utils/utils.dart';
import 'package:xplora/src/widgets/countdown.dart';
// import 'package:xplora/src/widgets/button_widgets.dart';
import 'package:xplora/src/widgets/pregunta_widget.dart';
// import 'package:xplora/src/widgets/questionSwiper_widget.dart';
import 'package:html2md/html2md.dart' as html2md;
// import 'package:flutter_html_widget/flutter_html_widget.dart';
class SimulacroPage extends StatefulWidget {
  @override
  _SimulacroPageState createState() => _SimulacroPageState();
 
}

class _SimulacroPageState extends State<SimulacroPage> {

  Timer _timer;
  int _start;
  bool alerta=true;
  PageController _pageController;
  int respuestaSeleccionada;
  int index=0;
  int temp=1;
  int temp0=0;
  int temp2=2;


  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    index=0;
    temp = 1;
    _pageController =
      new PageController(initialPage: index, viewportFraction: 1, keepPage: true);
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _pageController.dispose();
    _timer.cancel();
  }
  void _nextPage(int delta) {
    _pageController.animateToPage(
        index + delta,
        duration: const Duration(milliseconds: 300),
        curve: Curves.ease
    );
  }
  void _navigate(page){
    setState(() {
      index=page;
      temp = index+1;
      temp0=temp-1;
      temp2=temp+1;
    });
  }

  var areas = {
    "1": "CIENCIAS NATURALES",
    "2": "CIENCIAS SOCIALES",
    "3": "LECTURA CRITÍCA",
    "4": "MATEMATICAS",
    "5": "INGLES",
    // "6": "c"

  };
  var colors = {
    "CIENCIAS NATURALES" : Color.fromRGBO(242, 27, 87, 1),
    "CIENCIAS SOCIALES"  : Color.fromRGBO(122, 43, 134, 1),
    "LECTURA CRITÍCA"    : Color.fromRGBO(240, 170, 17, 1),
    "MATEMATICAS"        : Color.fromRGBO(21, 171, 159, 1),
    "INGLES"             : Color.fromRGBO(43, 120, 226, 1),
    "3"                  : Color.fromRGBO(122, 43, 141, 1),
    "4"                  : Color.fromRGBO(237, 74, 74, 1)  
  };
  
  BuildContext cont;
  SimulacroProvider simulacroProvider = new SimulacroProvider();
  SimulacroModel simulacro;
  FactionProvider factions;
  ProfileProvider profileProvider = new ProfileProvider();
  Size s;
  LoginBloc bloc;
  bool setTime= true;
  bool timeOver = true;
  // bool cargando;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    s = size;
    cont=context;
    simulacro = ModalRoute.of(context).settings.arguments;
    bloc      = ProviderLogin.of(context);
    if(setTime){
      bloc.type=='1'?_start=1500
                    :bloc.type=='2'?_start=300
                                   :_start=7500;
      setTime=false;
    }

//mostrarAlerta3(context, 'Ops, se te ha agotado el tiempo, para pruebas latam no es un problema, pero para las pruebas icfes ya te recogieron, maneja mejor tu tiempo.', 'El tiempo ha finalizado!');

    Future.delayed(new Duration(seconds: _start),(){
      if(timeOver){
        timeOver = false;
        return bloc.type=='2'?_contraRelojValidation(simulacro, bloc, context)
                           :mostrarAlerta3(context, 'Puedes continuar resolviendo el simulacro pero tu puntuación sera menor', 'El tiempo ha finalizado!');
      
      }
      
            //  bloc.type=='2'?
            //                :             
    });
    // startTimer();
    
    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
          appBar: _appBar(context, bloc),
          body: _questionSwiper( context,simulacro,),
          bottomNavigationBar: _bottomBar(context, bloc, simulacro),
        ));
  }

  Future<bool> _onBackPressed() {
    return showDialog(
        context: cont,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            title: Column(
              children: <Widget>[
                SizedBox(height: s.height * 0.03),
                Container(
                  height: s.height * 0.16,
                  child: Image(
                    image: AssetImage('assets/LatamColor.png'),
                  ),
                ),
                SizedBox(height: s.height * 0.05),
                Text(
                  '¿Quieres salir del simulacro?',
                  style: TextStyle(),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: s.height * 0.02,
                ),
              ],
            ),
            content: Text(
              'Si abandonas la prueba ahora perderás las monedas que gastaste, estás de acuerdo?',
              textAlign: TextAlign.center,
            ),
            actions: <Widget>[
              FlatButton(
                color: Colors.purple,
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(12.0)),
                child: Container(
                  alignment: Alignment.center,
                  height: s.height * 0.06,
                  width: s.width * 0.16,
                  child: Text('No',
                      style: Theme.of(context).textTheme.subhead.apply(
                            color: Colors.white,
                          )),
                ),
                onPressed: () => Navigator.pop(context, false),
              ),
              SizedBox(
                width: s.width * 0.05,
              ),
              FlatButton(
                color: Colors.red,
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(12.0)),
                child: Container(
                  alignment: Alignment.center,
                  height: s.height * 0.06,
                  width: s.width * 0.16,
                  child: Text(
                    'Si',
                    style: Theme.of(context).textTheme.subhead.apply(
                          color: Colors.white,
                        ),
                  ),
                ),
                onPressed: () {
                  alerta=false;
                  _updateCoins(bloc, context);
                }
              ),
              SizedBox(
                width: s.width * 0.10,
              )
            ],
          );
        });
  }
  _updateCoins(LoginBloc bloc, BuildContext context) async {  
      try{
        loadings(context);
        Map info = await profileProvider.actualizarCoins(bloc.pin);
        if (info['status']) {   
          Navigator.pushNamedAndRemoveUntil(context,'/', (Route<dynamic> route) => false);;
          bloc.setCoins(info['coins']);
        } else {
          Navigator.pop(context);
          mostrarAlerta(context, info['mensaje']);
        }
      } catch(e){
      } 
  }

  Widget _appBar(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return PreferredSize(
        preferredSize: Size.fromHeight(size.height * 0.18197),
        child: StreamBuilder(
          stream: bloc.factionStream,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            return SafeArea(
                child: Column(
              children: <Widget>[
                Container(
                  height: size.height * 0.1,
                  alignment: Alignment.center,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      // Container(
                      //   height: size.height * 0.06,
                      //   child: Image(
                      //     image: AssetImage(
                      //       bloc.faction.icon,
                      //     ),
                      //     fit: BoxFit.cover,
                      //   ),
                      // ),
                      SizedBox(
                        width: 5,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                              bloc.type =="1"?"SIMULACRO"
                              :bloc.type == "2"
                                  ? "CONTRARELOJ"
                                  : bloc.type == "3"
                                  ? 'PRE-ICFES'
                                  : 'PRUEBA PROGRAMADA',
                              style: TextStyle(
                                  color: 
                                    bloc.type =="1"||bloc.type == "2"?Color(bloc.faction.color):bloc.type == "3"?colors['3']:colors['4']
                                    ,
                                  fontSize: size.width * 0.06,
                                  fontWeight: FontWeight.w600)
                          ),
                          bloc.area=="6"?Divider(height: size.height*0.0001,)
                                         :Text(
                                            areas[bloc.area],
                                            style: TextStyle(color: colors[areas[bloc.area]]),
                                         ),
                          
                        ],
                      )
                    ],
                  ),
                ),
                Divider(
                  thickness: 2,
                  height: 1,
                ),
                Row(
                  // mainAxisSize: MainAxisSize.min,
                  
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    FlatButton(
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          
                          Icon(
                            Icons.navigate_before,
                            size: 25,
                            color: colors[areas[bloc.area]],
                          ),
                          Text(
                            'Anterior',
                            style: TextStyle(
                              color: bloc.type =="1"||bloc.type == "2"?colors[areas[bloc.area]]:bloc.type == "3"?colors['3']:colors['4'],
                            ),
                            ),
                        ],
                      ),
                      onPressed: (){
                        _nextPage(-1);
                      },
                    ),
                    Container(
                      child: Text(
                        '$temp',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 25
                        ),
                      ),
                      color: bloc.type =="1"||bloc.type == "2"?colors[areas[bloc.area]]:bloc.type == "3"?colors['3']:colors['4'],
                      width: size.width*0.09,
                      alignment: Alignment.center,
                    ),
                    FlatButton(
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            'Siguiente',
                            style: TextStyle(
                              color: bloc.type =="1"||bloc.type == "2"?colors[areas[bloc.area]]:bloc.type == "3"?colors['3']:colors['4'],
                            ),
                          ),
                          Icon(
                            Icons.navigate_next,
                            size: 25,
                            color: colors[areas[bloc.area]],
                          ),
                        ],
                      ),
                      onPressed: (){
                        _nextPage(1);
                      },
                    ),
                  ],
                ),
                Divider(
                  thickness: 1,
                  height: 0.5,
                ),
              ],
            ));
          },
        ));
  }

  Widget _bottomBar(BuildContext context, LoginBloc bloc, SimulacroModel simulacro) {
    var clock = AdvCountdown(futureDate: simulacro.simulacrumMaxTime, type: bloc.type,);
    final size = MediaQuery.of(context).size;
    print(simulacro.simulacrumMaxTime.toString());
    return Container(
      height: size.height * 0.09,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            blurRadius: 3,
          )
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          
            
          clock,
          
          nextButtom(context, 'feedback', bloc, simulacro)
        ],
      ),
    );
  }

  Widget _questionSwiper(BuildContext context,SimulacroModel simulacro) {
    final size = MediaQuery.of(context).size;
    return Container(
          height: size.height * 1.3,
          child: PageView.builder(
            physics: BouncingScrollPhysics(),
            itemBuilder:(BuildContext context, int indx){
              return _cards(context, simulacro.questions[indx]);
            },
            // pageSnapping: true,
            controller: _pageController,
            onPageChanged: (page){
              _navigate(page);
            },
            itemCount: simulacro.questions.length,
          ),
    );
  }

  Widget nextButtom(BuildContext context, String route, LoginBloc bloc,
      SimulacroModel simulacro) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.pinStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return FlatButton(
          child: Container(
            height: size.height * 0.05,
            width: size.width * 0.25,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                gradient: LinearGradient(
                  colors: [
                    Color.fromRGBO(122, 42, 141, 1),
                    Color.fromRGBO(237, 74, 76, 1)
                  ],
                )),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'FINALIZAR',
                  style: TextStyle(fontSize: 12, color: Colors.white),
                ),
                // Icon(
                //   Icons.navigate_next,
                //   color: Colors.white,
                //   size: size.width * 0.052,
                // ),
              ],
            ),
          ),
          onPressed: () {
            bloc.type=='2'?_contraRelojValidation(simulacro, bloc, context)
                          : _otherValidation(simulacro, bloc, context);
          },
        );
      },
    );
  }

  _contraRelojValidation(SimulacroModel simulacro, LoginBloc bloc, BuildContext context) async {
    
     
        
      Future.delayed(new Duration(seconds: 1),(){
        // loadings(context);    
        return _sendAnswers(simulacro, bloc, context);   
      });
        
      //   for (var i = 0; i < simulacro.questions.length; i++) {
      //   if (simulacro.questions[i].selectedAnswer == null) {
      //     return simulacroIncompleto(context, (i + 1).toString());
      //   }
      // }
      
    
  }

  bool complete = false;

  _otherValidation(SimulacroModel simulacro, LoginBloc bloc, BuildContext context) async { 
  try{
    loadings(context);    
    for (var i = 0; i < simulacro.questions.length; i++) {
    if (simulacro.questions[i].selectedAnswer == null) {
      Navigator.pop(context);
      return simulacroIncompleto(context, (i + 1).toString());
    } else {
      if(i == simulacro.questions.length-1){
        Navigator.pop(context);
        _sendAnswers(simulacro, bloc, context);
      }
    }
    }
    
  }catch(e){}   
 }
  
  _sendAnswers(SimulacroModel simulacro, LoginBloc bloc,BuildContext context)async{
    try{
      loadings(context);
      Map info = await simulacroProvider.validarSimulacro(
        bloc.pin, simulacro.simulacrumId, simulacro.toJson(), bloc);

      if (info['status']) {
        info['simulacro'] = simulacro;
        setState(() {
          alerta=false;
        });
        print('ff');
        Navigator.pop(context);
        Navigator.pushNamedAndRemoveUntil(context, 'feedback', ModalRoute.withName('/'), arguments: info);
      } else {
        Navigator.pop(context);
        mostrarAlerta(context, info['mensaje']);
      }
    }catch(e){

    }
  }
  Widget _cards(BuildContext context, Question pregunta) {
    // _currentIndex=i+1;
    final size = MediaQuery.of(context).size;
    final bloc = ProviderLogin.of(context);
    
    
    String markdown = html2md.convert(pregunta.content);
    return ListView(
        // mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          // SizedBox(height: size.height*0.025,),
        
          Container(
              padding: EdgeInsets.fromLTRB(size.height * 0.02, size.height * 0.025, size.height * 0.02, size.height * 0.02),
              alignment: Alignment.center,
              child: new MarkdownBody(
                data: markdown.replaceAll('&nbsp;', ' ').replaceAll('**', '').replaceAll('&amp', '&').replaceAll('&quot', ''),               
                // styleSheet: MarkdownStyleSheet(
                // ),
              )
          ),
          SizedBox(height: size.height*0.02,),
          Column(
            // mainAxisSize: MainAxisSize.min,
            children: _options(context, pregunta, bloc),
          )
          ,SizedBox(height: size.height*0.04,),
        ],
      );
    
  }

  List<Widget> _options(
      BuildContext context, Question options, LoginBloc bloc) {
    int x = 0;

    return options.answers.map((option) {
      x++;
      String currentValue = option.id;
      String markdown = html2md.convert(option.content);
      return RadioListTile(

          value: int.parse(option.id),
          title:RaisedButton(
            color: Colors.white,
            shape: RoundedRectangleBorder(
              side: BorderSide(
                color: options.selectedAnswer==int.parse(currentValue)?Color(bloc.faction.color):Colors.black26,
                width: 3
              ),
              borderRadius: new BorderRadius.circular(10.0),
            ),
            child: Container(
              padding: EdgeInsets.all(10),
              child: new MarkdownBody(
                  data: markdown.replaceAll('&nbsp;', ' '),               
                  // styleSheet: MarkdownStyleSheet(
                  // ),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: Colors.white
              ),
            ),
            onPressed: (){
              setState(() {
              options.selectedAnswer = int.parse(currentValue);
            });
            },
          ),
          //  Text(
          //   option.content.replaceAll('<sub>', '').replaceAll('</sub>', '').replaceAll('<p>', '').replaceAll('</p>', '').replaceAll('<br>', '').replaceAll('<div>', '').replaceAll('</div>', '').,
          // ),
          groupValue: options.selectedAnswer,
          activeColor: Color(bloc.faction.back_color),
          onChanged: (int value) {
            setState(() {
              options.selectedAnswer = value;
            });
          });
    }).toList();
  }
}
