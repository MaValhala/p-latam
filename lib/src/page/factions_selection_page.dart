import 'dart:io';

import 'package:flutter/material.dart';
import 'package:xplora/src/providers/factions_provider.dart';
import 'package:xplora/src/widgets/factions_pageView.dart';

class FactionsPageSelection extends StatefulWidget {

  @override
  _FactionsPageSelectionState createState() => _FactionsPageSelectionState();
}

class _FactionsPageSelectionState extends State<FactionsPageSelection> {
  final factionsProvider = new FactionProvider();



  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Container(
        height: size.height,
        width: size.width,
        child: _factions(context),
      )
    );
  }

  Widget _factions(BuildContext context){
    
    final size = MediaQuery.of(context).size;
    return SafeArea(
      child: Container(
        width: size.width,
        height: size.height*0.8,
        child: FutureBuilder(
          future: factionsProvider.cargarData(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
              if(snapshot.hasData){
              return FactionPageView(factions: snapshot.data,);
            }
              return CircularProgressIndicator();
          },
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    final s = MediaQuery.of(context).size;
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            title: Column(
              children: <Widget>[
                SizedBox(height: s.height * 0.03),
                Container(
                  height: s.height * 0.16,
                  child: Image(
                    image: AssetImage('assets/LatamColor.png'),
                  ),
                ),
                SizedBox(height: s.height * 0.04),
                Text(
                  '¡Espera!, selecciona una facción antes de salir',
                  style: TextStyle(),
                  textAlign: TextAlign.center,
                ),
                // SizedBox(
                //   height: s.height * 0.015,
                // ),
              ],
            ),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  'Solo podrás escoger facción una vez, si sales del registro ahora se te asignara una aleatoriamente, ¿Estás de acuerdo?',
                  textAlign: TextAlign.justify,
                  style: Theme.of(context).textTheme.subhead.apply(color: Colors.black54),
                ),
                SizedBox(
                  height: s.height * 0.025,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    FlatButton(
                      color: Colors.purple,
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(12.0)),
                      child: Container(
                        alignment: Alignment.center,
                        height: s.height * 0.06,
                        width: s.width * 0.16,
                        child: Text('No',
                            style: Theme.of(context).textTheme.subhead.apply(
                                  color: Colors.white,
                                )),
                      ),
                      onPressed: () => Navigator.pop(context, false),
                    ),
                    // SizedBox(width: 15,),
                    FlatButton(
                      color: Colors.red,
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(12.0)),
                      child: Container(
                        alignment: Alignment.center,
                        height: s.height * 0.06,
                        width: s.width * 0.16,
                        child: Text(
                          'Si',
                          style: Theme.of(context).textTheme.subhead.apply(
                                color: Colors.white,
                              ),
                        ),
                      ),
                      onPressed: () => Navigator.pushNamedAndRemoveUntil(context, 'login', (Route<dynamic> route) => false),
                    ),
                  ],
                ),
              ],
            )
          );
        });
  }
}