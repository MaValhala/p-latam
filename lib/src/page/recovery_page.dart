
import 'package:flutter/material.dart';
// import 'package:xplora/src/bloc/login_bloc.dart';
import 'package:xplora/src/providers/login_provider.dart';
import 'package:xplora/src/providers/simulacro_provider.dart';
// import 'package:xplora/src/utils/utils.dart';

class RecoveryPage extends StatefulWidget {

  @override
  _RecoveryPageState createState() => _RecoveryPageState();
}

class _RecoveryPageState extends State<RecoveryPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/PaginaLogin.png'), fit: BoxFit.cover)),
      child: _content(context),
    ));
  }

  SimulacroProvider _simulacroProvider = new SimulacroProvider();

  Widget _content(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Container(
          width: size.width * 0.95,
          child: _text(context),
        ),

        // _button(context)
      ],
    );
  }

  Widget _logo(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.35,
      width: size.width * 0.6,
      // margin: EdgeInsets.symmetric(horizontal: size.width * 0.2, vertical: 10),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(
                'assets/LatamLogin.png',
              ),
              fit: BoxFit.contain)),
    );
  }

  Widget _text(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        // textDirection: TextDirection.rtl,
        children: <Widget>[
          _logo(context),
          // Text(
          //   'Para pasar de una pregunta a otra recuerda deslizar tu dedo a la derecha o a la izquierda. También es importante saber que algunas preguntas tienen imágenes, estas puede tardar en mostrarse dependiendo de tu conexión a internet.',
          //   style:
          //       Theme.of(context).textTheme.subhead.apply(color: Colors.white),
          //   textAlign: TextAlign.justify,
          // ),
          SizedBox(
            height: size.height * 0.07,
          ),
          Container(
            width: size.width * 0.9,
            child: Text(
              '¡Enhorabuena!',
              style:
                  Theme.of(context).textTheme.headline.apply(color: Colors.white),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            height: size.height * 0.06,
          ),
          Container(
            width: size.width * 0.9,
            child: Text(
              'Se ha enviado un mensaje con una nueva contraseña a tu correo electrónico.',
              style:
                  Theme.of(context).textTheme.title.apply(color: Colors.white),
              textAlign: TextAlign.justify,
            ),
          ),
          SizedBox(
            height: size.height * 0.15,
          ),
          _button(context)
        ],
      ),
    );
  }

  Widget _button(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final bloc = ProviderLogin.of(context);
    return RaisedButton(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          child: Text(
            'ENTENDIDO',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16.0,
            ),
          ),
        ),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
        elevation: 0.0,
        color: Colors.white,
        textColor: Colors.purple,
        disabledTextColor: Colors.white10,
        onPressed: () {
          Navigator.pushNamedAndRemoveUntil(context, 'login', (Route<dynamic> route) => false);
        } /**TODO: Implementación boton INICIAR SESION */
        );
  }




   





}