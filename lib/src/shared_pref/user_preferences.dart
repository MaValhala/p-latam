import 'package:shared_preferences/shared_preferences.dart';

class UserPrefs{

  //ususario y contraseña para hacer el login auto
  static final UserPrefs _instancia = new UserPrefs._internal();


  factory UserPrefs(){
    return _instancia;
  }
  UserPrefs._internal();

  SharedPreferences prefs;

  initPrefs()async{
    prefs =await SharedPreferences.getInstance();
  }
  //getters y setters
  
  get user{
    return prefs.getString('user');
  }
  set user(String user){
    prefs.setString('user', user);
  }
  get password{
    return prefs.getString('password');
  }
  set password(String password){
    prefs.setString('password', password);
  }

}