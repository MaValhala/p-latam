//Verificar si una entrada es numerica//
import 'package:flutter/material.dart';
import 'dart:io' show Platform;

import 'package:store_redirect/store_redirect.dart';

bool isNumeric(String s) {
  if (s.isEmpty) return false;

  final n = num.tryParse(s);

  return (n == null) ? false : true;
}

void mostrarAlerta(BuildContext context, String mensaje) {
  final size = MediaQuery.of(context).size;
  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          title: Column(
            children: <Widget>[
              SizedBox(height: size.height * 0.03),
              Container(
                height: size.height * 0.16,
                child: Image(
                  image: AssetImage('assets/LatamColor.png'),
                ),
              ),
              SizedBox(height: size.height * 0.05),
              Text(
                'Opps, Ha ocurrido un error!',
                style: TextStyle(),
                textAlign: TextAlign.center,
              ),
              
            ],
          ),
          content:Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                mensaje,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: size.height*0.03,),
              FlatButton(
                  child: Container(
                    child: Text(
                      'Entendido',
                      style: TextStyle(color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                    alignment: Alignment.center,
                    height: size.height * 0.06,
                    width: size.width * 0.3,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        gradient: LinearGradient(
                          colors: [
                            Color.fromRGBO(122, 42, 141, 1),
                            Color.fromRGBO(237, 74, 76, 1)
                          ],
                        )),
                  ),
                  onPressed: () => Navigator.of(context).pop(),
                ),
            ],
          ) 
          
          // actions: <Widget>[
          //   Row(
          //     children: <Widget>[
          //       SizedBox(
          //         width: size.width * 0.19,
          //       ),
                
          //       SizedBox(
          //         width: size.width * 0.19,
          //       ),
          //     ],
          //   ),
          // ],
        );
      });
}
void versionAlert(BuildContext context) {
  final size = MediaQuery.of(context).size;
  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          title: Column(
            children: <Widget>[
              SizedBox(height: size.height * 0.03),
              Container(
                height: size.height * 0.16,
                child: Image(
                  image: AssetImage('assets/LatamColor.png'),
                ),
              ),
              SizedBox(height: size.height * 0.05),
              Text(
                '¡Hay una nueva actualización disponible!',
                style: TextStyle(),
                textAlign: TextAlign.center,
              ),
              
            ],
          ),
          content:Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                'Es importante realizar la actualización para el correcto funcionamiento de la aplicación.',
                textAlign: TextAlign.center,
              ),
              SizedBox(height: size.height*0.03,),
              FlatButton(
                  child: Container(
                    child: Text(
                      'Actualizar ahora',
                      style: TextStyle(color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                    alignment: Alignment.center,
                    height: size.height * 0.06,
                    width: size.width * 0.3,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        gradient: LinearGradient(
                          colors: [
                            Color.fromRGBO(122, 42, 141, 1),
                            Color.fromRGBO(237, 74, 76, 1)
                          ],
                        )),
                  ),
                  onPressed: () {
                    if(Platform.isAndroid||Platform.isIOS){
                      StoreRedirect.redirect(
                        androidAppId: 'com.pruebaslatam',
                        iOSAppId:'' // Insert ios app id
                      );
                    }else{
                      mostrarAlerta3(context, 'No hay una versión disponible para tu sistema operativo.', 'Sistema operativo incompatible');
                    }
                    Navigator.of(context).pop();
                  },
                ),
            ],
          ) 
          
          // actions: <Widget>[
          //   Row(
          //     children: <Widget>[
          //       SizedBox(
          //         width: size.width * 0.19,
          //       ),
                
          //       SizedBox(
          //         width: size.width * 0.19,
          //       ),
          //     ],
          //   ),
          // ],
        );
      });
}

void mostrarAlerta3(BuildContext context, String mensaje, String title) {
  final size = MediaQuery.of(context).size;
  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          title: Column(
            children: <Widget>[
              SizedBox(height: size.height * 0.03),
              Container(
                height: size.height * 0.16,
                child: Image(
                  image: AssetImage('assets/LatamColor.png'),
                ),
              ),
              SizedBox(height: size.height * 0.05),
              Text(
                title,
                style: TextStyle(),
                textAlign: TextAlign.center,
              ),
            ],
          ),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                mensaje,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: size.height*0.03,),
              FlatButton(
                  child: Container(
                    child: Text(
                      'Entendido',
                      style: TextStyle(color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                    alignment: Alignment.center,
                    height: size.height * 0.06,
                    width: size.width * 0.3,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        gradient: LinearGradient(
                          colors: [
                            Color.fromRGBO(122, 42, 141, 1),
                            Color.fromRGBO(237, 74, 76, 1)
                          ],
                        )),
                  ),
                  onPressed: () => Navigator.of(context).pop(),
                ),
            ],
          ) 
        );
      });
}
void mostrarAlerta5(BuildContext context, String mensaje) {
  final size = MediaQuery.of(context).size;
  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          title: Column(
            children: <Widget>[
              SizedBox(height: size.height * 0.03),
              Container(
                height: size.height * 0.16,
                child: Image(
                  image: AssetImage('assets/LatamColor.png'),
                ),
              ),
              // SizedBox(height: size.height * 0.05),
              // Text(
              //   title,
              //   style: TextStyle(),
              //   textAlign: TextAlign.center,
              // ),
            ],
          ),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                mensaje,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.title,
              ),
              SizedBox(height: size.height*0.03,),
              FlatButton(
                  child: Container(
                    child: Text(
                      'Entendido',
                      style: TextStyle(color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                    alignment: Alignment.center,
                    height: size.height * 0.06,
                    width: size.width * 0.3,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        gradient: LinearGradient(
                          colors: [
                            Color.fromRGBO(122, 42, 141, 1),
                            Color.fromRGBO(237, 74, 76, 1)
                          ],
                        )),
                  ),
                  onPressed: () => Navigator.of(context).pop(),
                ),
            ],
          ) 
        );
      });
}
bool mostrarAlerta4(BuildContext context, String mensaje, String title) {
  final size = MediaQuery.of(context).size;
  showDialog(
      context: context,
      useRootNavigator: true,
      builder: (context) {
        return AlertDialog(

          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          title: Column(
            children: <Widget>[
              SizedBox(height: size.height * 0.03),
              Container(
                height: size.height * 0.16,
                child: Image(
                  image: AssetImage('assets/LatamColor.png'),
                ),
              ),
              SizedBox(height: size.height * 0.05),
              Text(
                title,
                style: TextStyle(),
                textAlign: TextAlign.center,
              ),
            ],
          ),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                mensaje,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: size.height*0.03,),
              FlatButton(
                  child: Container(
                    child: Text(
                      'Entendido',
                      style: TextStyle(color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                    alignment: Alignment.center,
                    height: size.height * 0.06,
                    width: size.width * 0.3,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        gradient: LinearGradient(
                          colors: [
                            Color.fromRGBO(122, 42, 141, 1),
                            Color.fromRGBO(237, 74, 76, 1)
                          ],
                        )),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                    return true;
                    

                  } 
                ),
            ],
          ) 
        );
      });
}
//  void otro(BuildContext context) {
//     final size = MediaQuery.of(context).size;
//     AlertDialog alert = AlertDialog(
//       shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
//       content: new Container(
//         decoration: BoxDecoration(color: Colors.white),
//         height: size.height * 0.6,
//         child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
//           Container(
//             padding: EdgeInsets.only(bottom: 25, top: 10),
//               height: size.height * 0.17,
//               child: Image.asset(
//                 'assets/LatamColor.png',
//                 fit: BoxFit.cover,
//               )),
//           Container(
//               // height: size.height * 0.2,
//               child: Text(
//                   'Para pasar de una pregunta a otra recuerda deslizar tu dedo ya sea a la derecha o la izquierda. También es importante saber que algunas preguntas tienen imágenes. Estas pueden tardar en mostrarse dependiendo tu conexión de internet.',
//                   textAlign: TextAlign.justify,
//                   style: Theme.of(context).textTheme.subhead.apply(color: Colors.black54),
//               ),
//           ),
//           Container(
//               height: size.height * 0.2,
//               child: Image.asset(
//                 'assets/latam/loading_gif.gif',
//                 fit: BoxFit.cover,
//               ))
//         ]),
//         // Container(margin: EdgeInsets.only(top: size.height * 0.05), child: Text('Cargando', style: TextStyle(fontStyle: FontStyle.italic, fontSize: 15),))
//       ),
//     );

//     showDialog(
//         barrierDismissible: false,
//         context: context,
//         builder: (BuildContext context) {
//           return alert;
//         });
//   }
void simulacroIncompleto(BuildContext context, String pregunta) {
  final size = MediaQuery.of(context).size;
  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          title: Column(
            children: <Widget>[
              SizedBox(height: size.height * 0.03),
              Container(
                height: size.height * 0.16,
                child: Image(
                  image: AssetImage('assets/LatamColor.png'),
                ),
              ),
              SizedBox(height: size.height * 0.05),
              Text(
                'Espera, ¡Aún no has resuelto todo el cuestionario!',
                style: TextStyle(),
                textAlign: TextAlign.center,
              ),
            ],
          ),
          content: Text(
            'Has olvidado responder la pregunta $pregunta, por favor respondela antes de continuar',
            textAlign: TextAlign.center,
          ),
          actions: <Widget>[
            Row(
              children: <Widget>[
                // SizedBox(
                //   width: size.width * 0.2,
                // ),
                FlatButton(
                  child: Container(
                    child: Text(
                      'Entendido',
                      style: TextStyle(color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                    alignment: Alignment.center,
                    height: size.height * 0.06,
                    width: size.width * 0.3,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        gradient: LinearGradient(
                          colors: [
                            Color.fromRGBO(122, 42, 141, 1),
                            Color.fromRGBO(237, 74, 76, 1)
                          ],
                        )),
                  ),
                  onPressed: () => Navigator.of(context).pop(),
                ),
                SizedBox(
                  width: size.width * 0.19,
                ),
              ],
            ),
          ],
        );
      });
}

void mostrarAlerta2(BuildContext context) {
  final size = MediaQuery.of(context).size;
  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          title: Column(
            children: <Widget>[
              SizedBox(height: size.height * 0.05),
              Container(
                height: size.height * 0.14,
                child: Image(
                  image: AssetImage('assets/LatamColor.png'),
                ),
              ),
              SizedBox(height: size.height * 0.04),
              // Container(
              //   padding: EdgeInsets.all(6),
              //   decoration: BoxDecoration(
              //     gradient: LinearGradient(colors: [
              //       Colors.purple, Colors.orange
              //     ]),
              //     borderRadius: BorderRadius.circular(20)
              //   ),
              //   child: Icon(help, color: Colors.white,),
              // ),
              // SizedBox(height: size.height*0.05),
              Text(
                'Bienvenido a pruebas LATAM, aqui podrás escojer tu facción.',
                style: TextStyle(),
                textAlign: TextAlign.center,
              ),
            ],
          ),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              // Text('Bienvenido a pruebas LATAM, aqui podrás escojer tu facción.'),
              // SizedBox(height: size.height*0.03),
              Text(
                'Una facción es un grupo que representa una materia o módulo de materias que te darán puntos adicionales por responder correctamente.',
                textAlign: TextAlign.center,
              ),
              SizedBox(height: size.height * 0.04),
              Text(
                'Puedes escoger la facción una única vez y no podrás cambiarla, piensálo muy bien!.',
                textAlign: TextAlign.center,
              )
            ],
          ),
          actions: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              // crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                // SizedBox(
                //   width: size.width * 0.19,
                // ),
                FlatButton(
                  child: Container(
                    child: Text(
                      'Entendido',
                      style: TextStyle(color: Colors.white),
                    ),
                    alignment: Alignment.center,
                    height: size.height * 0.06,
                    width: size.width * 0.3,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        gradient: LinearGradient(
                          colors: [
                            Color.fromRGBO(122, 42, 141, 1),
                            Color.fromRGBO(237, 74, 76, 1)
                          ],
                        )),
                  ),
                  onPressed: () => Navigator.of(context).pop(),
                ),
                SizedBox(
                  width: size.width * 0.19,
                ),
              ],
            ),
            // SizedBox(height: size.height * 0.05),
          ],
        );
      });
}

void loadings(BuildContext context) {
  final size = MediaQuery.of(context).size;
  AlertDialog alert = AlertDialog(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    content: new Container(
        decoration: BoxDecoration(
          color: Colors.white
        ),
        height: size.height*0.5,
        width: size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: size.height*0.3,
              
              child: Image.asset(
              'assets/latam/loading_gif.gif',
              fit: BoxFit.cover,
              ),
              width: size.width,
              
            ),
            // Container(margin: EdgeInsets.only(top: size.height * 0.05), child: Text('Cargando', style: TextStyle(fontStyle: FontStyle.italic, fontSize: 15),))
          ]
        ),
      )
  );
  
  showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      });
}
void loadingsMessage(BuildContext context, String text) {
  final size = MediaQuery.of(context).size;
  AlertDialog alert = AlertDialog(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    content: new Container(
        decoration: BoxDecoration(
          color: Colors.white
        ),
        height: size.height*0.5,
        width: size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              height: size.height*0.3,
              
              child: Image.asset(
              'assets/latam/loading_gif.gif',
              fit: BoxFit.cover,
              ),
              width: size.width,
              
            ),
            SizedBox(height: size.height*0.05,),
            Text(
              text,
              style: TextStyle(
                fontSize: 18
              ),
              )
            
            // Container(margin: EdgeInsets.only(top: size.height * 0.05), child: Text('Cargando', style: TextStyle(fontStyle: FontStyle.italic, fontSize: 15),))
          ]
        ),
      ),
  );
  showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      });
}