class Factions {
  List<Faction> factions = new List();
  Factions();
  Factions.fromJsonList(List<dynamic> jsonList){
    if (jsonList==null)return;
    for (var item in jsonList){
      final pelicula= new Faction.fromJsonMap(item);
      factions.add(pelicula);
    };
  }
}
class Faction {
  String icon;
  String id;
  String name;
  String banner;
  String miniBanner;
  String logo_color;
  String rocket;
  int color;
  int green;
  int back_color;
  String destinyRoute;
  String background;
  
  

  Faction({
    this.icon,
    this.id,
    this.name,
    this.banner,
    this.miniBanner,
    this.destinyRoute,
    this.back_color,
    this.green,
    this.color,
    this.background,
    this.logo_color,
    this.rocket
  });
 Faction.fromJsonMap(Map<String,dynamic> json){
    name         =  json['name'];
    banner       =  json['banner'];
    miniBanner   =  json['mini_banner'];
    destinyRoute =  json['destiny_route'];  
    color        =  int.parse(json['color']);
    back_color   =  int.parse(json['back_color']);
    background   =  json['background'];
    logo_color   = json['logo_color'];
    rocket       = json['rocket'];
    id           = json['id'];
    icon         = json['icon'];
    
    }
  
}
