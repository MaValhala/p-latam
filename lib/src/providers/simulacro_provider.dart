// import 'package:flutter/services.dart' show rootBundle;

import 'dart:convert';

// import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart';
import 'package:xplora/src/bloc/login_bloc.dart';
import 'package:xplora/src/models/simulacro_model.dart';
import 'package:http/http.dart' as http;
import 'package:xplora/src/propierties/api_propierties.dart';
import 'package:xplora/src/utils/utils.dart';
// import 'package:xplora/src/providers/login_provider.dart';


class SimulacroProvider {

  SimulacroModel simulacro;
  final String _url =  HOST;
  

  Future<Map<String, dynamic>> validarSimulacro(String pin, String id, List<String> questions, LoginBloc bloc) async{
     
    final  authData = {
      'pin'                : pin,
      'simulacrum_id'      : id,
      'answers'            : questions.toString()
    };

    final resp = await http.post(
      '$_url/Simulacrum/Validation',
      // body: json.encode(authData) 
      body: authData
    );

    Map<String, dynamic> decodedResp = json.decode( resp.body..replaceAll('&lt;', '<').replaceAll('&gt;', '>').replaceAll('&amp;quot', '&').replaceAll("'´", '') );
    print('hola');
    if ( decodedResp['message']=='success'){
      //simulacro = new SimulacroModel.fromJson(decodedResp);
      bloc.changeCoins(decodedResp['coins'].toString());
      bloc.changePoints(decodedResp['points'].toString());
      return { 
        'status'    : true,
        'coins_won' : decodedResp['coins_won'],
        'points_won': decodedResp['points_won'],
        'result'    : decodedResp['result'],
        'answers'   : decodedResp['answers'],
      };
    } else {
      return { 'status': false, 'mensaje': decodedResp['error']};
    }

  }

  Future<Map<String, dynamic>> obtenerSimulacro(String pin, String area, String type, BuildContext context) async{
    
    final authData = {
      'pin'                : pin,
      'area_id'            : area,
      'type_simulacrum_id' : type
    };

    final resp = await http.post(
      '$_url/Simulacrum/Generate',
      body: authData 
    );
    if(resp.body==''){
      Navigator.pop(context);
      mostrarAlerta(context, 'No hemos podido generar la prueba, intentalo nuevamente.');
    
    }else{
      final resptemp = resp.body.replaceAll('&lt;', '<').replaceAll('&gt;', '>').replaceAll('&amp;quot', '&').replaceAll("'´", '');
      Navigator.pop(context);
      loadingsMessage(context, 'Cargando prueba...');
      
      Map<String, dynamic> decodedResp = json.decode( resptemp );
      print("");
      if ( decodedResp.containsKey('simulacrum_id')){
        simulacro = new SimulacroModel.fromJson(decodedResp);
        return { 
          'status': true,
        };
      } else {
        return { 'status': false, 'mensaje': decodedResp['error']};
      }
    }
    // print(resp.body);
    

  }


  Future<Map<String, dynamic>> obtenerSimulacro2(String pin, String area, String type, BuildContext context) async{
    
    final authData = {
      'pin'                : pin,
      'area_id'            : area,
      'type_simulacrum_id' : type
    };

    final resp = await http.post(
      '$_url/Simulacrum/Generate',
      body: authData 
    );
    if(resp.body==''){
      mostrarAlerta(context, 'Ha ocurriod un error al descargar la prueba, intenta nuevamente.');
    
    }else{
      Navigator.pop(context);
      loadingsMessage(context, 'Cargando prueba...');
      Map<String, dynamic> decodedResp = json.decode( resp.body.replaceAll('&lt;', '<').replaceAll('&gt;', '>') );
      if ( decodedResp.containsKey('simulacrum_id')&&decodedResp['message']!='error'){
        simulacro = new SimulacroModel.fromJson(decodedResp);
        return { 
          'status': true,

        };
      } else {
        return { 'status': false, 'message': decodedResp['error']};
      }
      }
    

  }

  Future<Map<String, dynamic>> revisionAnswer(String question_id, String answer_id, LoginBloc bloc) async{
    
    final authData = {
      'pin'                : bloc.pin,
      'answer_id'          : answer_id,
      'question_id'        : question_id
    };

    final resp = await http.post(
      '$_url/Correct_answer/explanation',
      body: authData 
    );

    Map<String, dynamic> decodedResp = json.decode( resp.body );
    if ( decodedResp.containsKey('explanation')){

      return { 
        'status': true,

      };
    } else {
      return { 'status': false, 'mensaje': decodedResp['error']};
    }

  }

 }


