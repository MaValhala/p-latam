import 'dart:async';

import 'package:xplora/src/bloc/validator.dart';
import 'package:rxdart/rxdart.dart';
import 'package:xplora/src/models/faction_model.dart';


class LoginBloc with Validators{

  final _emailController            = BehaviorSubject<String>();
  final _passwordController         = BehaviorSubject<String>();
  final _coinsController            = BehaviorSubject<String>();
  final _pointsController           = BehaviorSubject<String>();
  final _pinController              = BehaviorSubject<String>();
  final _factionController          = BehaviorSubject<Faction>();
  final _areaController             = BehaviorSubject<String>();
  final _typeController             = BehaviorSubject<String>();
  final _nameController             = BehaviorSubject<String>();
  final _surnameController          = BehaviorSubject<String>();
  final _lastNameController         = BehaviorSubject<String>();
  final _lastName2Controller        = BehaviorSubject<String>();
  final _phoneController            = BehaviorSubject<String>();
  final _documentTypeController     = BehaviorSubject<String>();
  final _documentController         = BehaviorSubject<String>();
  
  //Recuperar datos del Stream
  get emailStream           => _emailController.stream.transform( validarEmail ); //TODO: Safe in user preferences
  get passwordStream        => _passwordController.stream.transform( validarPassword ); //TODO: Safe in user prefences
  get coinsStream           => _coinsController.stream;
  get pointStream           => _pointsController.stream;
  get pinStream             => _pinController.stream;
  get factionStream         => _factionController.stream;
  get areaStream            => _areaController.stream;
  get typeStream            => _typeController.stream;
  get nameStream            => _nameController.stream;
  get surnameStream         => _surnameController.stream;
  get lastname1Stream       => _lastNameController.stream;
  get lastname2Stream       => _lastName2Controller.stream;
  get phoneStream           => _phoneController.stream;
  get documentTypeStream    => _documentTypeController.stream;
  get documentStream        => _documentController.stream;
  Stream<bool>get formValidStream  => Observable.combineLatest2(emailStream, passwordStream, (e, p)=> true);
  Stream<bool>get formValidFaction => Observable.combineLatest(factionStream, (p)=> true);
  
  
  

  
  //Insertar datos al Stream
  Function(String)  get changeEmail         => _emailController.sink.add;
  Function(String)  get changePassword      => _passwordController.sink.add;
  Function(String)  get changeCoins         => _coinsController.sink.add;
  Function(String)  get changePoints        => _pointsController.sink.add;
  Function(String)  get changePin           => _pinController.sink.add;
  Function(Faction) get changeFaction       => _factionController.sink.add;
  Function(String)  get changeArea          => _areaController.sink.add;
  Function(String)  get changeType          => _typeController.sink.add;
  Function(String)  get changeName          => _nameController.sink.add;
  Function(String)  get changeSurname       => _surnameController.sink.add;
  Function(String)  get changeLastName1     => _lastNameController.sink.add;
  Function(String)  get changeLastName2     => _lastName2Controller.sink.add;
  Function(String)  get changePhone         => _phoneController.sink.add;
  Function(String)  get changeDocument      => _documentTypeController.sink.add;
  Function(String)  get changeDocumentType  => _documentController.sink.add;
  void setCoins(String coins){
    changeCoins(coins);
  }

  void setPoints(String points){
    changePoints(points);
  }

  void setPin(String pin){
    changePin(pin);
  }

  void setFaction(Faction faction){
    changeFaction(faction);
  }
 


  //Obtener el último valor insertado al Stream
  String  get email         => _emailController.value;
  String  get password      => _passwordController.value;
  String  get coins         => _coinsController.value;
  String  get points        => _pointsController.value;
  String  get pin           => _pinController.value;
  Faction get faction       => _factionController.value; 
  String  get area          => _areaController.value;
  String  get type          => _typeController.value;
  String  get name          => _nameController.value;
  String  get surname       => _surnameController.value;
  String  get lastName1     => _lastNameController.value;
  String  get lastName2     => _lastName2Controller.value;
  String  get phone         => _phoneController.value;
  String  get documentType  => _documentController.value;
  String  get document      => _documentTypeController.value;
  dispose(){
    _emailController?.close();
    _passwordController?.close();
    _coinsController?.close();
    _pointsController?.close();
    _pinController?.close();
    _factionController?.close();
    _typeController?.close();
    _areaController?.close();
    _nameController?.close();
    _surnameController?.close();
    _lastNameController?.close();
    _lastName2Controller?.close();
    _phoneController?.close();
    _documentTypeController?.close();
    _documentController?.close();
  }
}